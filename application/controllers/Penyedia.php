<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Penyedia extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Penyedia_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'penyedia/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'penyedia/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'penyedia/index.html';
            $config['first_url'] = base_url() . 'penyedia/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Penyedia_model->total_rows($q);
        $penyedia = $this->Penyedia_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'penyedia_data' => $penyedia,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Pangkalan',
            'konten' => 'penyedia/penyedia_list',
            'atas'=>'page/atas',
            'bawah'=>'page/bawah'
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Penyedia_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_penyedia' => $row->id_penyedia,
		'penyedia' => $row->penyedia,
		'nama_penyedia' => $row->nama_penyedia,
		'alamat' => $row->alamat,
		'qty' => $row->qty,
		'sisa_temp' => $row->sisa_temp,
	    );
            $this->load->view('penyedia/penyedia_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyedia'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas'=>'page/atas',
            'bawah'=>'page/bawah',
            'judul_page' => 'Data Pangkalan',
            'konten' => 'penyedia/penyedia_form',
            'button' => 'Create',
            'action' => site_url('penyedia/create_action'),
	    'id_penyedia' => set_value('id_penyedia'),
	    'penyedia' => set_value('penyedia'),
	    'nama_penyedia' => set_value('nama_penyedia'),
        'no_telp' => set_value('no_telp'),
	    'alamat' => set_value('alamat'),
	    'qty' => set_value('qty'),
	    'sisa_temp' => set_value('sisa_temp'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'penyedia' => $this->input->post('penyedia',TRUE),
		'nama_penyedia' => $this->input->post('nama_penyedia',TRUE),
        'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'sisa_temp' => $this->input->post('sisa_temp',TRUE),
	    );

            $this->Penyedia_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('penyedia'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Penyedia_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas'=>'page/atas',
                'bawah'=>'page/bawah',
                'judul_page' => 'Data Pangkalan',
                'konten' => 'penyedia/penyedia_form',
                'button' => 'Update',
                'action' => site_url('penyedia/update_action'),
		'id_penyedia' => set_value('id_penyedia', $row->id_penyedia),
		'penyedia' => set_value('penyedia', $row->penyedia),
		'nama_penyedia' => set_value('nama_penyedia', $row->nama_penyedia),
        'no_telp' => set_value('no_telp', $row->no_telp),
		'alamat' => set_value('alamat', $row->alamat),
		'qty' => set_value('qty', $row->qty),
		'sisa_temp' => set_value('sisa_temp', $row->sisa_temp),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyedia'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_penyedia', TRUE));
        } else {
            $data = array(
		'penyedia' => $this->input->post('penyedia',TRUE),
		'nama_penyedia' => $this->input->post('nama_penyedia',TRUE),
        'no_telp' => $this->input->post('no_telp',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'sisa_temp' => $this->input->post('sisa_temp',TRUE),
	    );

            $this->Penyedia_model->update($this->input->post('id_penyedia', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('penyedia'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Penyedia_model->get_by_id($id);

        if ($row) {
            $this->Penyedia_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('penyedia'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('penyedia'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('penyedia', 'penyedia', 'trim|required');
	$this->form_validation->set_rules('nama_penyedia', 'nama penyedia', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');
	// $this->form_validation->set_rules('qty', 'qty', 'trim|required');

	$this->form_validation->set_rules('id_penyedia', 'id_penyedia', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Penyedia.php */
/* Location: ./application/controllers/Penyedia.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-01-25 05:23:23 */
/* https://jualkoding.com */