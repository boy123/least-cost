<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class App extends CI_Controller {

    public $image = '';
    
    public function index()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'atas' => '',
            'bawah' => '',
            'konten' => 'home_admin',
            'judul_page' => 'Dashboard',
        );
        $this->load->view('v_index', $data);
    }

    public function biaya()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'generate_biaya',
            'judul_page' => 'Generate Biaya',
        );
        $this->load->view('v_index', $data);
    }   

    public function selesaikan_perhitungan($tgl)
    {
        $this->db->where('penyedia', 'dummy');
        $this->db->delete('penyedia');

        $this->db->where('status', 'proses');
        $this->db->update('permintaan', ['status'=>'selesai']);
        
        $this->db->where('tanggal', $tgl);
        $this->db->update('hasil_least_cost', ['status'=>'selesai']);

        $this->db->where('tanggal', $tgl);
        $hasil = $this->db->get('hasil_least_cost');
        foreach ($hasil->result() as $rw) {
            //kurangi stok
            $this->db->query("UPDATE penyedia SET qty = qty - '$rw->qty' where penyedia='$rw->penyedia' ");
        }

        $this->session->set_flashdata('message', alert_biasa('Perhitungan berhasil diselesaikan','success'));
        redirect("app/riwayat_perhitungan");
    }

    public function riwayat_perhitungan()
    {
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'riwayat_perhitungan',
            'judul_page' => 'Riwayat Perhitungan Biaya',
        );
        $this->load->view('v_index', $data);
    }   

    public function hasil_least_cost()
    {
        error_reporting(0);
        if ($this->session->userdata('level') == '') {
            redirect('login');
        }
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'hasil_least_cost',
            'judul_page' => 'Hasil Least Cost',
        );
        $this->load->view('v_index', $data);
    }  

    public function hapus_least_cost($tgl)
    {
        $this->db->where('tanggal', $tgl);
        $this->db->delete('hasil_least_cost');

        $this->session->set_flashdata('message', alert_biasa('Berhasil di hapus','success'));
        redirect("app/riwayat_perhitungan");
    }

    public function generate_biaya()
    {
        // $this->db->truncate('biaya');

        //cek permintaan yang sudah di approve
        $this->db->where('status', 'proses');
        $cek = $this->db->get('permintaan');
        if ($cek->num_rows() == 0) {
            $this->session->set_flashdata('message', alert_biasa('Tidak ada permintaan berstatus PROSES','error'));
            redirect("app/biaya");
            exit();
        }

        $tgl = $_POST['tanggal'];

        $this->db->where('tgl_buat', $tgl);
        $this->db->delete('biaya');

        $sum_py = $this->db->query("SELECT SUM(qty) as a FROM penyedia")->row()->a;
        $sum_pr = $this->db->query("SELECT SUM(qty) as a FROM permintaan WHERE status='proses' ")->row()->a;
        
        $this->db->where('penyedia', 'dummy');
        $this->db->delete('penyedia');

        $this->db->where('toko', 'dummy');
        $this->db->where('tgl_hitung', $tgl);
        $this->db->delete('permintaan');

        if ($sum_pr > $sum_py) {
            $sisa = $sum_pr - $sum_py;
            $this->db->insert('penyedia', array(
                'penyedia' => 'dummy',
                'nama_penyedia' => 'dummy',
                'qty'=>$sisa
            ));
        } else {
            $sisa = $sum_py - $sum_pr;
            $this->db->insert('permintaan', array(
                'toko' => 'dummy',
                'status' => 'proses',
                'qty'=>$sisa
            ));
        }

        $penyedia = $this->db->get('penyedia');
        foreach ($penyedia->result() as $rw) {
            $this->db->where('status', 'proses');
            $tokos = $this->db->get('permintaan');
            foreach ($tokos->result() as $toko) {

                $this->db->where('penyedia', $rw->penyedia);
                $this->db->where('toko', $toko->toko);
                $this->db->order_by('tgl_buat', 'desc');
                $biaya = $this->db->get('biaya');

                $this->db->insert('biaya', array(
                    'penyedia' => $rw->penyedia,
                    'toko' => $toko->toko,
                    'tgl_buat' => $tgl,
                    'biaya' => $biaya->row()->biaya,
                    'jarak' => $biaya->row()->jarak
                ));
            }

        }
        $this->session->set_flashdata('message', alert_biasa('Berhasil di generate','success'));
        redirect("app/biaya");
    }  

    public function update_biaya($tgl)
    {
        $this->db->where('tgl_buat', $tgl);
        foreach ($this->db->get('biaya')->result() as $rw) {
            $this->db->where('id_biaya', $rw->id_biaya);
            $this->db->update('biaya', array(
                'biaya' => $_POST['biaya'][$rw->id_biaya],
                'jarak' => $_POST['jarak'][$rw->id_biaya]
            ));
        }
        $this->session->set_flashdata('message', alert_biasa('Data berhasil di update','success'));
        redirect("app/biaya");
    }

    public function run()
    {
        $tgl = $this->input->post('tanggal');

        // log_r($tgl);

        //cek permintaan yang sudah di approve
        $this->db->where('status', 'proses');
        $cek = $this->db->get('permintaan');
        if ($cek->num_rows() == 0) {
            $this->session->set_flashdata('message', alert_biasa('Tidak ada permintaan berstatus PROSES','error'));
            redirect("app/hasil_least_cost");
            exit();
        }

        $cek_is_biaya = $this->db->get_where('biaya', ['tgl_buat'=>$tgl]);
        if ($cek_is_biaya->num_rows() == 0) {
            $this->session->set_flashdata('message', alert_biasa('Data biaya tanggal '.$tgl.' tidak di temukan ! ','error'));
            redirect("app/hasil_least_cost");
            exit();
        }


        //cek biaya apakah ada null
        $cek_biaya = $this->db->query("SELECT * FROM biaya where biaya IS NULL and tgl_buat='$tgl' ");
        if ($cek_biaya->num_rows() > 0) {
            $this->session->set_flashdata('message', alert_biasa('Data biaya tidak boleh kosong','error'));
            redirect("app/hasil_least_cost");
            exit();
        }


        //record stok penyedia
        $this->db->query("DELETE FROM rekam_stok_hitung WHERE tgl_hitung='$tgl' ");
        foreach ($this->db->query("SELECT penyedia,qty FROM penyedia")->result() as $penyedia) {
            $this->db->insert('rekam_stok_hitung', array(
                'penyedia' => $penyedia->penyedia,
                'qty' => $penyedia->qty,
                'tgl_hitung' => $tgl
            ));
        }
        $this->db->query("DELETE FROM hasil_least_cost WHERE tanggal='$tgl' ");
        $this->reset($tgl);

        // do {
        //     log_data("sedang proses..");
        //     // $this->least_cost();
        //     // $this->least_cost_new();
        // } while ($this->db->query("SELECT * FROM penyedia WHERE sisa_temp > 0")->num_rows() > 0);

        $this->proses_cek($tgl);

        $this->reset($tgl);
        $this->session->set_flashdata('message', alert_biasa('Kalkulasi berhasil dilakukan','success'));
        redirect("app/hasil_least_cost");
    }

    public function reset($tgl)
    {
        $this->db->query("UPDATE penyedia SET sisa_temp=qty");
        $this->db->query("UPDATE permintaan SET sisa_temp=qty, tgl_hitung='$tgl' WHERE status='proses' ");
        $this->db->query("UPDATE biaya SET arsir='n' WHERE tgl_buat='$tgl' ");

    }

    public function least_cost()
    {
        $tgl = date('Y-m-d');

        $persediaan = $this->db->query("SELECT * FROM penyedia WHERE sisa_temp > 0")->result();
        foreach ($persediaan as $rw) {

            //mencari biaya terkecil
            $terkecil = $this->db->query("SELECT min(biaya) as kecil FROM biaya WHERE penyedia='$rw->penyedia' and tgl_buat='$tgl' ")->row()->kecil;

            $sql = "
                select a.penyedia, a.biaya, a.toko 
                FROM biaya as a
                INNER JOIN permintaan as b on a.toko=b.toko
                where a.biaya='$terkecil' and a.penyedia='$rw->penyedia' and a.tgl_buat='$tgl' and b.sisa_temp > 0
            ";

            $biaya_terkecil = $this->db->query($sql);


            //jika biaya terkecil sisa_temp = 0, maka cari sisa_temp yg besar dari 0

            if ($biaya_terkecil->num_rows() > 0) {
                $tokos = $biaya_terkecil->row();

                //cek permintaan banding persediaan
                $permintaan = $this->db->get_where('permintaan', ['toko'=>$tokos->toko])->row();
                $sisa = $rw->sisa_temp - $permintaan->sisa_temp;

                //masukkan ke hasil least cost
                if ($rw->sisa_temp > $permintaan->sisa_temp) {
                    $this->db->insert('hasil_least_cost', array(
                        'penyedia' => $rw->penyedia,
                        'qty' => $permintaan->sisa_temp,
                        'toko' => $tokos->toko,
                        'tanggal' => $tgl
                    ));
                } else {
                    $this->db->insert('hasil_least_cost', array(
                        'penyedia' => $rw->penyedia,
                        'qty' => $rw->sisa_temp,
                        'toko' => $tokos->toko,
                        'tanggal' => $tgl
                    ));
                }

                

                //log_data("$rw->penyedia $tokos->toko $sisa");

                if ($sisa < 0) {
                    $this->db->where('penyedia', $rw->penyedia);
                    $this->db->update('penyedia', ['sisa_temp'=>0]);
                    $this->db->where('toko', $tokos->toko);
                    $this->db->update('permintaan', ['sisa_temp'=>abs($sisa)]);
                } else {
                    $this->db->where('penyedia', $rw->penyedia);
                    $this->db->update('penyedia', ['sisa_temp'=>$sisa]);
                    $this->db->where('toko', $tokos->toko);
                    $this->db->update('permintaan', ['sisa_temp'=>0]);
                }

            } else {
                // cari permintaan yg sisa_temp > 0

                $sql_perm = "SELECT * FROM permintaan WHERE sisa_temp > 0 and status='proses' ";
                $cek_perm = $this->db->query($sql_perm);
                if ($cek_perm->num_rows() > 0) {
                    $pr = $cek_perm->row();
                    //cek permintaan banding persediaan
                    $sisa = $rw->sisa_temp - $pr->sisa_temp;

                    if ($rw->sisa_temp > $pr->sisa_temp) {
                        $this->db->insert('hasil_least_cost', array(
                            'penyedia' => $rw->penyedia,
                            'qty' => $pr->sisa_temp,
                            'toko' => $pr->toko,
                            'tanggal' => $tgl
                        ));
                    } else {
                        $this->db->insert('hasil_least_cost', array(
                            'penyedia' => $rw->penyedia,
                            'qty' => $rw->sisa_temp,
                            'toko' => $pr->toko,
                            'tanggal' => $tgl
                        ));
                    }

                    //log_data("$rw->penyedia $pr->toko $sisa");

                    if ($sisa < 0) {
                        $this->db->where('penyedia', $rw->penyedia);
                        $this->db->update('penyedia', ['sisa_temp'=>0]);
                        $this->db->where('toko', $pr->toko);
                        $this->db->update('permintaan', ['sisa_temp'=>abs($sisa)]);
                    } else {
                        $this->db->where('penyedia', $rw->penyedia);
                        $this->db->update('penyedia', ['sisa_temp'=>$sisa]);
                        $this->db->where('toko', $pr->toko);
                        $this->db->update('permintaan', ['sisa_temp'=>0]);
                    }
                } else {
                    echo "selesai";
                }
               
                
                

            }

            

        }
    }

    public function proses_cek($tgl)
    {

        do{

            $dt_biaya = $this->db->query("
                SELECT
                    a.*,
                    b.qty as persediaan_qty,
                    b.sisa_temp as sisa_persediaan,
                    c.qty as permintaan_qty,
                    c.sisa_temp as sisa_permintaan,
                    c.id_permintaan
                FROM
                    biaya a 
                    INNER JOIN penyedia b ON a.penyedia = b.penyedia
                    INNER JOIN permintaan c ON a.toko=c.toko
                WHERE
                    a.arsir = 'n' 
                    AND a.tgl_buat = '$tgl' 
                    AND c.status = 'proses'
                ORDER BY
                    a.biaya ASC")->row();

            //persediaan - permintaan
            $hasil = $dt_biaya->sisa_persediaan - $dt_biaya->sisa_permintaan;
            if ($hasil > 0) {
                log_data("proses 1..");

                 //set hasil least cost
                $this->db->insert('hasil_least_cost', array(
                    'penyedia' => $dt_biaya->penyedia,
                    'qty' => $dt_biaya->sisa_permintaan,
                    'toko' => $dt_biaya->toko,
                    'tanggal'=> $tgl
                ));

                //update permintaan menjadi 0 atau habis
                $this->db->where('id_permintaan', $dt_biaya->id_permintaan);
                $this->db->update('permintaan', ['sisa_temp'=>0]);

                //update sisa persediaan
                $this->db->where('penyedia', $dt_biaya->penyedia);
                $this->db->update('penyedia', ['sisa_temp'=>$hasil]);

               

                //cari toko mana dan set arsir di biaya menjadi  'y'
                $this->db->where('toko', $dt_biaya->toko);
                $this->db->update('biaya', ['arsir'=>'y']);

            } else {
                log_data("proses 2..");

                //set hasil least cost
                $this->db->insert('hasil_least_cost', array(
                    'penyedia' => $dt_biaya->penyedia,
                    'qty' => $dt_biaya->sisa_persediaan,
                    'toko' => $dt_biaya->toko,
                    'tanggal'=> $tgl
                ));

                //update sisa permintaan dan set $hasil menjadi positif
                $this->db->where('id_permintaan', $dt_biaya->id_permintaan);
                $this->db->update('permintaan', ['sisa_temp'=>abs($hasil)]);

                //update sisa persediaan menjadi 0 atau habis
                $this->db->where('penyedia', $dt_biaya->penyedia);
                $this->db->update('penyedia', ['sisa_temp'=>0]);

                //cari penyedia mana dan set arsir di biaya menjadi  'y'
                $this->db->where('penyedia', $dt_biaya->penyedia);
                $this->db->update('biaya', ['arsir'=>'y']);
            }
            // log_data($hasil.' | '.$dt_biaya->persediaan_qty.' | '.$dt_biaya->permintaan_qty);
            // log_data("proses..");


        } while($this->db->query("SELECT * FROM biaya WHERE arsir='n' and tgl_buat='$tgl' ORDER BY biaya ASC")->num_rows() > 0);

        log_data("selesai");
        

    }

    public function tambah_permintaan_toko()
    {
        if ($_POST) {
            $kode_toko = get_data('toko','id_toko',$this->session->userdata('id_join'),'toko');
            $this->db->insert('permintaan', array(
                'toko' => $kode_toko,
                'qty' => $this->input->post('qty'),
                'tanggal' => $this->input->post('tanggal')
            ));
            $this->session->set_flashdata('message', alert_biasa('Permintaan berhasil dibuat','success'));
            redirect('app/list_permintaan_toko','refresh');
        } else {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'konten' => 'user_toko/tambah_permintaan_toko',
                'judul_page' => 'Tambah Permintaan Toko',
            );
            $this->load->view('v_index', $data);
        }
    }

    public function list_permintaan_toko()
    {
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'user_toko/list_permintaan_toko',
            'judul_page' => 'List Permintaan Toko',
        );
        $this->load->view('v_index', $data);
    }

    public function update_persediaan_pangkalan()
    {
        if ($this->session->userdata('id_join') == '') {
            $this->session->set_flashdata('message', alert_biasa('terjadi kesalahan silahkan logout dahulu','success'));
            redirect('app/list_persediaan_pangkalan','refresh');
        }
        if ($_POST) {
            $kode = get_data('penyedia','id_penyedia',$this->session->userdata('id_join'),'penyedia');
            $this->db->insert('history_persediaan', array(
                'penyedia' => $kode,
                'qty' => $this->input->post('qty'),
                'tanggal' => $this->input->post('tanggal')
            ));
            $qty = $this->input->post('qty');
            $tanggal = $this->input->post('tanggal');
            $this->db->query("UPDATE penyedia SET qty = qty + '$qty', tanggal='$tanggal' where penyedia='$kode' ");

            $this->session->set_flashdata('message', alert_biasa('Persediaan berhasil dibuat','success'));
            redirect('app/list_persediaan_pangkalan','refresh');
        } else {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'konten' => 'user_pangkalan/update_persediaan_pangkalan',
                'judul_page' => 'Persediaan',
            );
            $this->load->view('v_index', $data);
        }
    }

    public function hapus_history_persediaan($id,$qty,$penyedia)
    {
        $this->db->where('id', $id);
        $this->db->delete('history_persediaan');

        $this->db->query("UPDATE penyedia SET qty = qty - '$qty' where penyedia='$penyedia' ");
        $this->session->set_flashdata('message', alert_biasa('qty persediaan berhasil dihapus','success'));
            redirect('app/list_persediaan_pangkalan','refresh');
    }

    public function list_persediaan_pangkalan()
    {
        if ($this->session->userdata('id_join') == '') {
            $this->session->set_flashdata('message', alert_biasa('terjadi kesalahan silahkan logout dahulu','success'));
            redirect('app/list_persediaan_pangkalan','refresh');
        }
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'user_pangkalan/list_persediaan_pangkalan',
            'judul_page' => 'Persediaan',
        );
        $this->load->view('v_index', $data);
    }

    public function data_distribusi_pangkalan()
    {
        $data = array(
            'atas' => 'page/atas',
            'bawah' => 'page/bawah',
            'konten' => 'user_pangkalan/data_distribusi_pangkalan',
            'judul_page' => 'Distribusi',
        );
        $this->load->view('v_index', $data);
    }

    public function update_profil()
    {
        if ($_POST) {
            $kode = get_data('penyedia','id_penyedia',$this->session->userdata('id_join'),'penyedia');
            $data = array(
                'nama_lengkap' => $this->input->post('nama_lengkap',TRUE),
                'username' => $this->input->post('username',TRUE),
                'password' => $retVal = ($this->input->post('password') == '') ? $_POST['password_old'] : md5($this->input->post('password',TRUE)),
                'level' => $this->input->post('level',TRUE),
                'foto' => $retVal = ($_FILES['foto']['name'] == '') ? $_POST['foto_old'] : upload_gambar_biasa('user', 'image/user/', 'jpeg|png|jpg|gif', 10000, 'foto')
            );
            $this->A_user_model->update($this->input->post('id_user', TRUE), $data);
            $this->session->set_flashdata('message', alert_biasa('Profile berhasil diupdate','success'));
            redirect('app','refresh');
        } else {
            $data = array(
                'atas' => 'page/atas',
                'bawah' => 'page/bawah',
                'konten' => 'update_profil',
                'judul_page' => 'Profil',
            );
            $this->load->view('v_index', $data);
        }
    }

    public function pengembangan()
    {
        $this->session->set_flashdata('message', alert_biasa('Sedang dalam pengembangan','info'));
        redirect("app");
    }

    
    

    

    
}
