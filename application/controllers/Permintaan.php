<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permintaan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Permintaan_model');
        $this->load->library('form_validation');
    }

    public function reset()
    {
        $this->db->query("DELETE FROM permintaan WHERE status!='selesai' ");
        $this->session->set_flashdata('message', alert_biasa('Data Berhasil di hapus','success'));
        redirect("permintaan");
    }

    public function approve($n, $id_permintaan)
    {
        if ($n == 'y') {
            $this->db->where('id_permintaan', $id_permintaan);
            $this->db->update('permintaan', ['status'=>'proses']);
        } else {
            $this->db->where('id_permintaan', $id_permintaan);
            $this->db->update('permintaan', ['status'=>'input']);
        }
        $this->session->set_flashdata('message', alert_biasa('Status permintaan berhasil diupdate','success'));
        redirect("permintaan");
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'permintaan/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'permintaan/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'permintaan/index.html';
            $config['first_url'] = base_url() . 'permintaan/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Permintaan_model->total_rows($q);
        $permintaan = $this->Permintaan_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'permintaan_data' => $permintaan,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'atas'=>'page/atas',
            'bawah'=>'page/bawah',
            'judul_page' => 'List Permintaan',
            'konten' => 'permintaan/permintaan_list',
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_permintaan' => $row->id_permintaan,
		'toko' => $row->toko,
		'qty' => $row->qty,
		'sisa_temp' => $row->sisa_temp,
		'created_at' => $row->created_at,
	    );
            $this->load->view('permintaan/permintaan_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas'=>'page/atas',
            'bawah'=>'page/bawah',
            'judul_page' => 'List Permintaan',
            'konten' => 'permintaan/permintaan_form',
            'button' => 'Create',
            'action' => site_url('permintaan/create_action'),
	    'id_permintaan' => set_value('id_permintaan'),
	    'toko' => set_value('toko'),
	    'qty' => set_value('qty'),
	    'sisa_temp' => set_value('sisa_temp'),
	    'created_at' => set_value('created_at'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'toko' => $this->input->post('toko',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'sisa_temp' => $this->input->post('sisa_temp',TRUE),
		'created_at' => get_waktu(),
	    );

            $this->Permintaan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('permintaan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas'=>'page/atas',
            'bawah'=>'page/bawah',
                'judul_page' => 'List Permintaan',
                'konten' => 'permintaan/permintaan_form',
                'button' => 'Update',
                'action' => site_url('permintaan/update_action'),
		'id_permintaan' => set_value('id_permintaan', $row->id_permintaan),
		'toko' => set_value('toko', $row->toko),
		'qty' => set_value('qty', $row->qty),
		'sisa_temp' => set_value('sisa_temp', $row->sisa_temp),
		'created_at' => set_value('created_at', $row->created_at),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_permintaan', TRUE));
        } else {
            $data = array(
		'toko' => $this->input->post('toko',TRUE),
		'qty' => $this->input->post('qty',TRUE),
		'sisa_temp' => $this->input->post('sisa_temp',TRUE),
	    );

            $this->Permintaan_model->update($this->input->post('id_permintaan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('permintaan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);

        if ($row) {
            $this->Permintaan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('permintaan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('toko', 'toko', 'trim|required');
	$this->form_validation->set_rules('qty', 'qty', 'trim|required');
	

	$this->form_validation->set_rules('id_permintaan', 'id_permintaan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Permintaan.php */
/* Location: ./application/controllers/Permintaan.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-01-25 05:31:06 */
/* https://jualkoding.com */