<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Toko extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Toko_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'toko/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'toko/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'toko/index.html';
            $config['first_url'] = base_url() . 'toko/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Toko_model->total_rows($q);
        $toko = $this->Toko_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'toko_data' => $toko,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Toko',
            'konten' => 'toko/toko_list',
            'atas'=>'page/atas',
            'bawah'=>'page/bawah'
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Toko_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_toko' => $row->id_toko,
		'toko' => $row->toko,
		'alamat' => $row->alamat,
	    );
            $this->load->view('toko/toko_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('toko'));
        }
    }

    public function create() 
    {
        $data = array(
            'judul_page' => 'Data Toko',
            'atas'=>'page/atas',
            'bawah'=>'page/bawah',
            'konten' => 'toko/toko_form',
            'button' => 'Create',
            'action' => site_url('toko/create_action'),
	    'id_toko' => set_value('id_toko'),
        'toko' => set_value('toko'),
	    'nama_toko' => set_value('nama_toko'),
        'alamat' => set_value('alamat'),
	    'no_telp' => set_value('no_telp'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
        'toko' => $this->input->post('toko',TRUE),
		'nama_toko' => $this->input->post('nama_toko',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Toko_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('toko'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Toko_model->get_by_id($id);

        if ($row) {
            $data = array(
                'judul_page' => 'Data Toko',
                'atas'=>'page/atas',
            'bawah'=>'page/bawah',
                'konten' => 'toko/toko_form',
                'button' => 'Update',
                'action' => site_url('toko/update_action'),
		'id_toko' => set_value('id_toko', $row->id_toko),
        'toko' => set_value('toko', $row->toko),
		'nama_toko' => set_value('nama_toko', $row->nama_toko),
        'alamat' => set_value('alamat', $row->alamat),
		'no_telp' => set_value('no_telp', $row->no_telp),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('toko'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_toko', TRUE));
        } else {
            $data = array(
        'toko' => $this->input->post('toko',TRUE),
		'nama_toko' => $this->input->post('nama_toko',TRUE),
        'alamat' => $this->input->post('alamat',TRUE),
		'no_telp' => $this->input->post('no_telp',TRUE),
	    );

            $this->Toko_model->update($this->input->post('id_toko', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('toko'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Toko_model->get_by_id($id);

        if ($row) {
            $this->Toko_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('toko'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('toko'));
        }
    }

    public function _rules() 
    {
    $this->form_validation->set_rules('toko', 'toko', 'trim|required');
	$this->form_validation->set_rules('nama_toko', 'nama toko', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id_toko', 'id_toko', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Toko.php */
/* Location: ./application/controllers/Toko.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-01-20 12:49:28 */
/* https://jualkoding.com */