<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Distributor extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Distributor_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $q = urldecode($this->input->get('q', TRUE));
        $start = intval($this->input->get('start'));
        
        if ($q <> '') {
            $config['base_url'] = base_url() . 'distributor/index.html?q=' . urlencode($q);
            $config['first_url'] = base_url() . 'distributor/index.html?q=' . urlencode($q);
        } else {
            $config['base_url'] = base_url() . 'distributor/index.html';
            $config['first_url'] = base_url() . 'distributor/index.html';
        }

        $config['per_page'] = 10;
        $config['page_query_string'] = TRUE;
        $config['total_rows'] = $this->Distributor_model->total_rows($q);
        $distributor = $this->Distributor_model->get_limit_data($config['per_page'], $start, $q);

        $this->load->library('pagination');
        $this->pagination->initialize($config);

        $data = array(
            'distributor_data' => $distributor,
            'q' => $q,
            'pagination' => $this->pagination->create_links(),
            'total_rows' => $config['total_rows'],
            'start' => $start,
            'judul_page' => 'Data Distributor',
            'konten' => 'distributor/distributor_list',
            'atas'=>'page/atas',
            'bawah'=>'page/bawah'
        );
        $this->load->view('v_index', $data);
    }

    public function read($id) 
    {
        $row = $this->Distributor_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_distributor' => $row->id_distributor,
		'distributor' => $row->distributor,
		'alamat' => $row->alamat,
	    );
            $this->load->view('distributor/distributor_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('distributor'));
        }
    }

    public function create() 
    {
        $data = array(
            'atas'=>'page/atas',
            'bawah'=>'page/bawah',
            'judul_page' => 'Data Distributor',
            'konten' => 'distributor/distributor_form',
            'button' => 'Create',
            'action' => site_url('distributor/create_action'),
	    'id_distributor' => set_value('id_distributor'),
	    'distributor' => set_value('distributor'),
	    'alamat' => set_value('alamat'),
	);
        $this->load->view('v_index', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'distributor' => $this->input->post('distributor',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Distributor_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('distributor'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Distributor_model->get_by_id($id);

        if ($row) {
            $data = array(
                'atas'=>'page/atas',
            'bawah'=>'page/bawah',
                'judul_page' => 'Data Distributor',
                'konten' => 'distributor/distributor_form',
                'button' => 'Update',
                'action' => site_url('distributor/update_action'),
		'id_distributor' => set_value('id_distributor', $row->id_distributor),
		'distributor' => set_value('distributor', $row->distributor),
		'alamat' => set_value('alamat', $row->alamat),
	    );
            $this->load->view('v_index', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('distributor'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_distributor', TRUE));
        } else {
            $data = array(
		'distributor' => $this->input->post('distributor',TRUE),
		'alamat' => $this->input->post('alamat',TRUE),
	    );

            $this->Distributor_model->update($this->input->post('id_distributor', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('distributor'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Distributor_model->get_by_id($id);

        if ($row) {
            $this->Distributor_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('distributor'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('distributor'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('distributor', 'distributor', 'trim|required');
	$this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

	$this->form_validation->set_rules('id_distributor', 'id_distributor', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Distributor.php */
/* Location: ./application/controllers/Distributor.php */
/* Please DO NOT modify this information : */
/* Generated by Boy Kurniawan 2022-01-20 12:49:20 */
/* https://jualkoding.com */