<?php 
$id = $this->session->userdata('id_user');
$this->db->where('id_user', $id);
$rw = $this->db->get('a_user')->row();
 ?>

<form action="" method="post" enctype="multipart/form-data">
	<div class="form-group">
        <label for="varchar">Full Name</label>
        <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" placeholder="Nama Lengkap" value="<?php echo $rw->nama_lengkap; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Username </label>
        <input type="text" class="form-control" name="username" id="username" placeholder="Username" value="<?php echo $rw->username; ?>" />
    </div>
    <div class="form-group">
        <label for="varchar">Password </label>
        <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="" />
        <div>
            *) Kosongkan, jika tidak dirubah
        </div>
        <input type="hidden" name="password_old" value="<?php echo $rw->password ?>">
    </div>

    <div class="form-group">
        <label for="varchar">Foto </label>
        <input type="file" class="form-control" name="foto" id="foto" placeholder="Foto" value="<?php echo $rw->foto; ?>" required/>
        <input type="hidden" name="foto_old" value="<?php echo $rw->foto ?>">
        <div>
            <?php if ($rw->foto != ''): ?>
                <b>*) Foto Sebelumnya : </b><br>
                <img src="image/user/<?php echo $rw->foto ?>" style="width: 100px;">
            <?php endif ?>
        </div>
    </div>
    <input type="hidden" name="id_user" value="<?php echo $rw->id_user ?>">

    <div class="form-group">
        <button type="submit" class="btn btn-primary">Update</button>
    </div>

</form>