<!doctype html>
<html lang="en" class="fullscreen-bg">

<head>
	<title>Login | SIGAS</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<!-- VENDOR CSS -->
	<link rel="stylesheet" href="assetnew/css/bootstrap.min.css">
	<link rel="stylesheet" href="assetnew/vendor/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="assetnew/vendor/linearicons/style.css">
	<!-- MAIN CSS -->
	<link rel="stylesheet" href="assetnew/css/main.css">
	<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
	<link rel="stylesheet" href="assetnew/css/demo.css">
	<!-- GOOGLE FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
	<!-- ICONS -->
	<link rel="apple-touch-icon" sizes="76x76" href="assetnew/img/apple-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="assetnew/img/favicon.png">
</head>

<body>
	
	<!-- WRAPPER -->
	<div id="wrapper">
		<div class="vertical-align-wrap">
			<div class="vertical-align-middle">
				<div class="auth-box ">
					<div class="left">
						<div class="content">
							<div class="header">
								<div class="logo text-center"><a href=""><img src="assetnew/img/sigas.png"></a></div>
								<p class="lead">Login to your account</p>
							</div>
							<form class="form-auth-small" action="<?php echo base_url() ?>login/aksi_login" method="post">
								<div class="form-group">
									<input type="text" class="form-control" id="nik" name="username" placeholder="Username">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="signin-password" name="password" placeholder="Password">
								</div>
								<div class="form-group clearfix">
									<label class="fancy-checkbox element-left">
										<input type="checkbox">
										<span>Remember me</span>
									</label>
								</div>
								<button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
								<div class="bottom">
									<!-- <span class="helper-text">Belum punya akun? Silahkan  <a href="daftar.php">Daftar</a></span> -->
								</div>
							</form>
						</div>
					</div>
					<div class="right">
						<div class="overlay"></div>
						<div class="content text">
							<h1 class="heading">Sistem Informasi Optimasi Pendistribusian Gas Elpiji 3 Kg</h1><br>
							<p>by: Baiq Rokyatul Jannah</p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- END WRAPPER -->
</body>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 <script type="text/javascript"><?php echo $this->session->userdata('message') ?></script>

</html>
