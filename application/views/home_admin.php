
<div class="row">
	<div class="col-md-12">
		<div class="alert alert-success">
			<h2>Selamat Datang kembali, <?php echo $this->session->userdata('username'); ?></h2>
		</div>
	</div>

</div>

<?php if ($this->session->userdata('level') == 'admin'): ?>
<div class="row">
	<div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-aqua">
        <div class="inner">
          <h4>Biaya Transportasi</h4>
        </div>
        <a href="app/biaya" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-green">
        <div class="inner">
          <h4>Pangkalan</h4>
        </div>
        <a href="penyedia" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-yellow">
        <div class="inner">
          <h4>Permintaan</h4>
        </div>
        <a href="permintaan" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-xs-6">
      <!-- small box -->
      <div class="small-box bg-red">
        <div class="inner">
          <h4>Riwayat Perhitungan</h4>
        </div>
        <a href="app/riwayat_perhitungan" class="small-box-footer">Lihat <i class="fa fa-arrow-circle-right"></i></a>
      </div>
    </div>
</div>
<?php endif ?>

<div class="row">
	<div class="col-md-12">
		<div class="alert alert-info">
			Sigas merupakan Sistem Informasi Optimasi Pendistribusian Gas Elpiji yang digunakan pada agen PT Hidayat Energi Putratama. Sistem ini digunakan untuk menghitung biaya transportasi dari pendistribusian gas elpiji agar diperoleh biaya transportasi yang optimal.
		</div>
	</div>
</div>


