<div class="row">

	<div class="col-md-12">
		<a href="app/update_persediaan_pangkalan" class="btn btn-info">Update Persediaan</a><br>
		<table class="table table-bordered" id="example2">
			<thead>
				<tr>
					<td>No.</td>
					<td>Jumlah Persediaan</td>
					<td>Tanggal</td>
					<td>Pilihan</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$kode = get_data('penyedia','id_penyedia',$this->session->userdata('id_join'),'penyedia');
				$this->db->where('penyedia', $kode);
				$this->db->order_by('tanggal', 'desc');
				foreach ($this->db->get('history_persediaan')->result() as $rw): ?>
					<tr>
						<td><?php echo $no ?></td>
						<td><?php echo $rw->qty ?></td>
						<td><?php echo tanggal_indo($rw->tanggal) ?></td>
						<td>
							<a href="app/hapus_history_persediaan/<?php echo $rw->id.'/'.$rw->qty.'/'.$kode ?>" onclick="javasciprt: return confirm('Are You Sure ?')" class="label label-danger">Hapus</a>
						</td>
					</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
	</div>
</div>