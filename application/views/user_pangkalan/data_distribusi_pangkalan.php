<div class="row">

	<div class="col-md-12">
		<table class="table table-bordered" id="example2">
			<thead>
				<tr>
					<td>No.</td>
					<td>Tanggal</td>
					<td>Tujuan</td>
					<td>Jumlah Distribusi</td>
					<td>Biaya Distribusi</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$kode = get_data('penyedia','id_penyedia',$this->session->userdata('id_join'),'penyedia');
				$this->db->where('penyedia', $kode);
				$this->db->order_by('tanggal', 'desc');
				foreach ($this->db->get('hasil_least_cost')->result() as $rw): ?>
					<tr>
						<td><?php echo $no ?></td>
						<td><?php echo tanggal_indo($rw->tanggal) ?></td>
						<td><?php echo get_data('toko','toko',$rw->toko,'nama_toko') ?></td>
						<td><?php echo $rw->qty ?></td>
						<td>
							<?php 
							$this->db->where('penyedia', $kode);
							$this->db->where('toko', $rw->toko);
							$biaya = $this->db->get('biaya');
							if ($biaya->num_rows() > 0) {
								echo 'Rp. '.number_format($biaya->row()->biaya * $rw->qty);
							} else {
								echo "Rp. 0";
							}
							 ?>
							
						</td>
						
					</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
	</div>
</div>