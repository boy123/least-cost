<form action="" method="POST">
	<div class="form-group">
		<label>Jumlah Permintaan</label>
		<input type="number" name="qty" class="form-control" required>
	</div>

	<div class="form-group">
		<label>Tanggal</label>
		<input type="date" name="tanggal" class="form-control" required>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-primary">Simpan</button>
	</div>
</form>