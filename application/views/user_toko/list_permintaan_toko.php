<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="example2">
			<thead>
				<tr>
					<td>No.</td>
					<td>Jumlah Permintaan</td>
					<td>Tanggal</td>
					<td>Status</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$this->db->order_by('tanggal', 'desc');
				$kode_toko = get_data('toko','id_toko',$this->session->userdata('id_join'),'toko');
				$this->db->where('toko', $kode_toko);
				$this->db->order_by('tanggal', 'desc');
				foreach ($this->db->get('permintaan')->result() as $rw): ?>
					<tr>
						<td><?php echo $no ?></td>
						<td><?php echo $rw->qty ?></td>
						<td><?php echo tanggal_indo($rw->tanggal) ?></td>
						<td>
							<?php if ($rw->status == 'input'): ?>
								<span class="label label-info">INPUT</span>
							<?php endif ?>
							<?php if ($rw->status == 'proses'): ?>
								<span class="label label-warning">DIPROSES</span>
							<?php endif ?>
							<?php if ($rw->status == 'selesai'): ?>
								<span class="label label-success">SELESAI</span>
							<?php endif ?>
						</td>
					</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
	</div>
</div>