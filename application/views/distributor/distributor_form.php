
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="distributor">Distributor <?php echo form_error('distributor') ?></label>
            <textarea class="form-control" rows="3" name="distributor" id="distributor" placeholder="Distributor"><?php echo $distributor; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <input type="hidden" name="id_distributor" value="<?php echo $id_distributor; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('distributor') ?>" class="btn btn-default">Cancel</a>
	</form>
   