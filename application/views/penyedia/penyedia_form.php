
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">No Registrasi <?php echo form_error('penyedia') ?></label>
            <input type="text" class="form-control" name="penyedia" id="penyedia" placeholder="Penyedia" value="<?php echo $penyedia; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Nama Distributor <?php echo form_error('nama_penyedia') ?></label>
            <input type="text" class="form-control" name="nama_penyedia" id="nama_penyedia" placeholder="Nama Distributor" value="<?php echo $nama_penyedia; ?>" />
        </div>

        <div class="form-group">
            <label for="varchar">No Telp </label>
            <input type="text" class="form-control" name="no_telp" id="no_telp" placeholder="No Telp" value="<?php echo $no_telp; ?>" required/>
        </div>
	    <div class="form-group">
            <label for="alamat">Alamat <?php echo form_error('alamat') ?></label>
            <textarea class="form-control" rows="3" name="alamat" id="alamat" placeholder="Alamat"><?php echo $alamat; ?></textarea>
        </div>
	    <!-- <div class="form-group">
            <label for="int">Persediaan <?php echo form_error('qty') ?></label>
            <input type="text" class="form-control" name="qty" id="qty" placeholder="Qty" value="<?php echo $qty; ?>" />
        </div> -->
	    <input type="hidden" name="id_penyedia" value="<?php echo $id_penyedia; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('penyedia') ?>" class="btn btn-default">Cancel</a>
	</form>
   