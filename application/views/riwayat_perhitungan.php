<div class="row">
	<div class="col-md-12">
		<table class="table table-bordered" id="example2">
			<thead>
				<tr>
					<th>No.</th>
					<th>Tanggal</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$sql = "SELECT * FROM hasil_least_cost GROUP BY tanggal";
				foreach ($this->db->query($sql)->result() as $rw): ?>
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo tanggal_indo($rw->tanggal) ?></td>
					<td>
						<?php if ($rw->status != 'selesai'): ?>
							<a href="app/selesaikan_perhitungan/<?php echo $rw->tanggal ?>" onclick="javasciprt: return confirm('Are You Sure ?')" class="label label-success">Selesai</a>
						<?php endif ?>
						<a href="app/hasil_least_cost/<?php echo $rw->tanggal ?>" class="label label-info">Detail</a>
						<a href="app/hapus_least_cost/<?php echo $rw->tanggal ?>" onclick="javasciprt: return confirm('Are You Sure ?')" class="label label-danger">Hapus</a>
					</td>
				</tr>
				<?php $no++; endforeach ?>
			</tbody>
		</table>
	</div>
</div>