<?php 
$tgl = date('Y-m-d');
if (isset($_GET['tgl'])) {
	$tgl = $_GET['tgl'];
}
 ?>
<div class="row">
	<div class="col-md-12">

		<div class="row">
			<div class="col-md-4">
				<form action="app/biaya" method="GET">
					<input type="date" name="tgl" value="<?php echo $tgl ?>" class="form-control" required>
					<button type="submit" class="btn btn-flat btn-info">Tampilkan</button>
				</form>
			</div>

			<div class="col-md-4">
				<form action="app/generate_biaya" method="POST">
					<input type="date" name="tanggal" class="form-control" required>
					<button type="submit" class="btn btn-primary">Generate Biaya</button>
				</form>
			</div>
		</div>
		
		<br><br>

		<form action="app/update_biaya/<?php echo $tgl ?>" method="POST">
		<table class="table table-bordered table-stripped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Penyedia</th>
					<th>Biaya</th>
					<th>Toko</th>
					<th>Jarak (KM)</th>
					<th>Tanggal</th>
				</tr>
			</thead>
			<tbody>
				<?php 
				$no = 1;
				$this->db->where('tgl_buat', $tgl);
				
				foreach ($this->db->get('biaya')->result() as $rw): ?>
					
				
				<tr>
					<td><?php echo $no ?></td>
					<td><?php echo get_data('penyedia','penyedia',$rw->penyedia,'nama_penyedia') ?></td>
					<td><input type="text" id="biaya<?php echo $rw->id_biaya ?>" name="biaya[<?php echo $rw->id_biaya ?>]" value="<?php echo $rw->biaya ?>"></td>
					<td><?php echo $retVal = ($rw->toko == 'dummy') ? 'dummy' : get_data('toko','toko',$rw->toko,'nama_toko') ; ?></td>
					<td><input type="text" id="jarak<?php echo $rw->id_biaya ?>" name="jarak[<?php echo $rw->id_biaya ?>]" value="<?php echo $rw->jarak ?>" onkeyup="setJarak('<?php echo $rw->id_biaya ?>')"></td>
					<td>
						<?php echo $rw->tgl_buat ?>
					</td>
				</tr>
				<?php $no++; endforeach ?>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="4"></td>
					<td><button type="submit" class="btn btn-primary">Update</button></td>
				</tr>
			</tfoot>
		</table>

		</form>
	</div>
</div>
<script type="text/javascript">
	
	function setJarak(id) {
		var jarak = $("#jarak"+id).val();
		var hasil = parseFloat(jarak) * 100;

		$("#biaya"+id).val( Math.round(parseFloat(hasil)) );

	}

</script>