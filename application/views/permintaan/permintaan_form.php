
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Toko <?php echo form_error('toko') ?></label>
            <select class="form-control select2" name="toko">
                <option value="<?php echo $toko ?>"><?php echo $toko ?></option>
                <?php 
                $sql = "SELECT * FROM toko WHERE toko NOT IN (SELECT toko FROM permintaan)";
                foreach ($this->db->query($sql)->result() as $rw): ?>
                    <option value="<?php echo $rw->toko ?>"><?php echo $rw->toko.' - '.$rw->nama_toko ?></option>
                <?php endforeach ?>
            </select>
        </div>
	    <div class="form-group">
            <label for="int">Qty Permintaan <?php echo form_error('qty') ?></label>
            <input type="text" class="form-control" name="qty" id="qty" placeholder="Qty" value="<?php echo $qty; ?>" />
        </div>
	    <input type="hidden" name="id_permintaan" value="<?php echo $id_permintaan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('permintaan') ?>" class="btn btn-default">Cancel</a>
	</form>
   