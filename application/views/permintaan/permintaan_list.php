
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <a href="permintaan/reset" onclick="javasciprt: return confirm('Are You Sure ?')" class="btn btn-warning">Reset</a>
                <?php //echo anchor(site_url('permintaan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-1 text-right">
            </div>
            <div class="col-md-3 text-right">
                
            </div>
        </div>
        <div class="table-responsive">
        <table class="table table-bordered" style="margin-bottom: 10px" id="example2">
            <thead>
            <tr>
                <th>No</th>
		<th>Toko</th>
		<th>Qty</th>
        <th>Tanggal</th>
		<th>Status</th>
		<th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $this->db->order_by('id_permintaan', 'desc');
            $permintaan_data = $this->db->get('permintaan');
            foreach ($permintaan_data->result() as $permintaan)
            {
                ?>
                <tr>
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $retVal = ($permintaan->toko == 'dummy') ? 'dummy' : $permintaan->toko.' - '.get_data('toko','toko',$permintaan->toko,'nama_toko') ; ?></td>
			<td><?php echo $permintaan->qty ?></td>
			<td><?php echo $permintaan->tanggal ?></td>
            <td>
                <?php if ($permintaan->status == 'input'): ?>
                    <span class="label label-info">INPUT</span>
                <?php endif ?>
                <?php if ($permintaan->status == 'proses'): ?>
                    <span class="label label-warning">DIPROSES</span>
                <?php endif ?>
                <?php if ($permintaan->status == 'selesai'): ?>
                    <span class="label label-success">SELESAI</span>
                <?php endif ?>
            </td>
			<td style="text-align:center" width="200px">

                <?php if ($permintaan->status == 'input'): ?>
                    <a href="permintaan/approve/y/<?php echo $permintaan->id_permintaan ?>" class="btn btn-xs btn-success">Setujui</a>
                <?php endif ?>

                <?php if ($permintaan->status == 'proses'): ?>
                    <a href="permintaan/approve/t/<?php echo $permintaan->id_permintaan ?>" class="label label-warning">Batal</a>
                <?php endif ?>

                <?php 
                if ($permintaan->status != 'selesai') {
                    echo anchor(site_url('permintaan/delete/'.$permintaan->id_permintaan),'<span class="label label-danger">Hapus</span>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                }
                 ?>

				<?php 
				// echo anchor(site_url('permintaan/update/'.$permintaan->id_permintaan),'<span class="label label-info">Ubah</span>'); 
				// echo ' | '; 
				
				?>
			</td>
		</tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        </div>
        
    