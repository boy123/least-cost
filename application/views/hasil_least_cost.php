<?php 
$total_pr = 0;
$total_se = 0;
$btn_aktif = 'aktif';
$where = "";
if ($this->uri->segment(3) !='') {
	$tgl = $this->uri->segment(3);
	$btn_aktif = 'non';
} else {
	$tgl = date('Y-m-d');
}

if (isset($_GET['tgl'])) {
	$tgl = $_GET['tgl'];
}

 ?>

<div class="row">
	<div class="col-md-12">
		<?php if ($btn_aktif == 'aktif'): ?>
			<div class="row" style="margin-bottom: 10px">
				<div class="col-md-4">
					<form action="app/hasil_least_cost" method="GET">
						<input type="date" name="tgl" class="form-control" value="<?php echo $tgl ?>" required><br>
						<button type="submit" class="btn btn-flat btn-info">Tampilkan</button>
					</form>
				</div>
				<div class="col-md-4">
					<?php 
					$this->db->where('status', 'proses');
        			$cek_no_finish = $this->db->get('hasil_least_cost');
        			if ($cek_no_finish->num_rows() > 0) {
        				$konfirm = 'onclick="javasciprt: return confirm(\'Masih ada permintaan dan perhitungan sebelumnya yang belum difinishkan, yakin akan menimpa perhitungan sebelumnya ?\')"';

        			} else {
        				$konfirm = "";
					 
        			}
        			?>
					<form action="app/run" method="POST">
						<input type="date" name="tanggal" class="form-control" required><br>
						<button type="submit" <?php echo $konfirm; ?> class="btn btn-flat btn-success"><i class="fa fa-spinner"></i> Proses Kalkulasi</button>
					</form>
				</div>
			</div>

		<?php else: ?>

			<h3>Hasil Perhitungan <?php echo $tgl ?></h3>
			

		<?php endif ?>
		
		<br><br>
		<table class="table table-bordered">
			<thead>
				<tr class="alert alert-info">
					<td style="background-color: white; color: black;">#</td>
				<?php foreach ($this->db->query("SELECT * from permintaan where tgl_hitung='$tgl' and status!='input' ")->result() as $pr): ?>
					<td class="text-center"><b><?php echo $retVal = ($pr->toko == 'dummy') ? 'dummy' : get_data('toko','toko',$pr->toko,'nama_toko') ; ?></b></td>
				<?php endforeach ?>
					<td>
						<b>Persediaan</b>
					</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($this->db->get_where('rekam_stok_hitung', ['tgl_hitung'=>$tgl])->result() as $py): ?>
				<tr class="alert alert-warning">
					<td><b><?php echo $retVal = ($py->penyedia == 'dummy') ? 'dummy' : get_data('penyedia','penyedia',$py->penyedia,'nama_penyedia') ; ?></b></td>

					<?php 
					foreach ($this->db->query("SELECT * from permintaan where tgl_hitung='$tgl' and status!='input' ")->result() as $pr): ?>
						
						<td class="text-center">

							
							<?php
							$hasil = $this->db->get_where('hasil_least_cost', ['penyedia'=>$py->penyedia,'toko'=>$pr->toko,'tanggal'=>$tgl]);
							if ($hasil->num_rows() > 0) {
								echo '<span style="color: black">'.$hasil->row()->qty .'</span> | ';
							} 

							 ?>
							
							
							<span>
								<?php 
								$b = $this->db->get_where('biaya', ['penyedia'=>$py->penyedia,'toko'=>$pr->toko,'tgl_buat'=>$tgl])->row();
								echo $b->biaya;

								 ?>
							</span>
								
						</td>

						

					<?php endforeach ?>
					<td class="text-right">
						<?php echo $py->qty; $total_se=$total_se+$py->qty ?>
					</td>

				</tr>
				<?php endforeach ?>
				<tr class="alert alert-success">
					<td><b>Permintaan</b></td>

					<?php 
					foreach ($this->db->query("SELECT * from permintaan where tgl_hitung='$tgl' and status!='input' ")->result() as $pr): ?>
						
						<td class="text-center">
							<?php echo $pr->qty; $total_pr = $total_pr + $pr->qty; ?>
						</td>

					<?php endforeach ?>
					<td class="text-center">
						<span class="text-bold">
							<?php echo $total_pr.' | '.$total_se ?>
						</span>
					</td>

				</tr>
			</tbody>
		</table>
	</div>
</div>
<div class="row" style="margin-top: 20px;">
	<div class="col-md-12">
		<p>
			<b>
				Hasil = 
				<?php
				$i = 0;
				$a = 0;
				$nums = $this->db->get_where('hasil_least_cost', ['tanggal'=>$tgl])->num_rows();
				foreach ($this->db->get_where('hasil_least_cost',['tanggal'=>$tgl])->result() as $hsl): ?>

					<?php 

					$c = $hsl->qty * $this->db->get_where('biaya', ['penyedia'=>$hsl->penyedia,'toko'=>$hsl->toko,'tgl_buat'=>$tgl])->row()->biaya; 
					$a = $a + $c; 

					?>

					( <?php echo $hsl->qty ?> * <?php echo $this->db->get_where('biaya', ['penyedia'=>$hsl->penyedia,'toko'=>$hsl->toko,'tgl_buat'=>$tgl])->row()->biaya; ?> )
					<?php if ($i < $nums-1): ?>
						 +
					<?php endif ?>


				<?php $i++; endforeach ?>
			</b>
		</p>
		<p>
			<b>

				Hasil &nbsp;&nbsp; = &nbsp;&nbsp;&nbsp;&nbsp; <span style="font-size: 20px;"><?php echo number_format($a) ?></span>

			</b>
		</p>
	</div>
</div>
<div class="row" style="margin-top: 100px;">
	<div class="col-md-12">
		<b>Waktu Pemrosesan : <strong>{elapsed_time}</strong> Detik.</b>
	</div>
</div>